import sys

def ff(n):
    return [0x00, 0x5f, 0x87, 0xaf, 0xd7, 0xff][n]

def light(r, g, b):
    return (.21 * r + .71 * g + .07 * b >= 128)

def c():
    columns = 6
    rows = 216 / columns
    for row in range(0, rows):
        items = []
        for col in range(0, columns):

            if True:
                c = col * rows + row
                red = ff(c // 36)
                green = ff((c // 6) % 6)
                blue = ff(c % 6)
            else:
                red = ff(row // 6)
                green = ff(row % 6)
                blue = ff(col)
                c = 36 * (row // 6) + 6 * (row % 6) + col

            fgcolor = (0, 0, 0) if light(red, green, blue) else (1, 1, 1)

            name = '%02x%02x%02x' % (red, green, blue)
            alt = '%d' % (16 + c,)
            items.append('\\c{%(r)f %(g)f %(b)f}'
                             '{%(fr)f %(fg)f %(fb)f}'
                             '{%(name)s}{%(alt)s}'
                             % {'r': red / 255.0,
                                'g': green / 255.0,
                                'b': blue /  255.0,
                                'fr': fgcolor[0], 'fg': fgcolor[1],
                                'fb': fgcolor[2],
                                'name': name,
                                'alt': alt})
            
        
        sys.stdout.write('%s\\cr\n' % ('&'.join(items),))


if __name__ == '__main__':
    c()

