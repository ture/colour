XETEX = xetex
PYTHON = python
VERSION := $(shell git describe --always --dirty)

colour.pdf: colour.tex coldata.tex 
	$(XETEX) \\def\\version{$(VERSION)} \\input colour

coldata.tex: colours.py
	$(PYTHON) colours.py >tmp
	mv tmp coldata.tex

clean:
	$(RM) colour.pdf coldata.tex colour.log


